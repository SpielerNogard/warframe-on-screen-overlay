import ast
import os


class Baubare_sachen(object):
    def __init__(self):
        
        self.itemname = "1223"
        self.items=[]
        self.load_items()
        self.add_thing()

    def load_items(self):
        filename = "Items.txt"
        exist = os.path.exists(filename)
        if exist == True:
            datalist=open("Items.txt","r")
            filedaten=datalist.read()
            datalist.close()
            datenliste = ast.literal_eval(filedaten)
            self.items = datenliste
            print("Items geladen")
            if len(self.items) == 0:
                self.add_thing()
        else:
            print("Items existiert noch nicht")
            datei = open("Items.txt","w")
            liste = []
            datei.write("[]")
            datei.close()
            print("Items wurde erstellt")

    def add_thing(self):
        while self.itemname != "ende":
            Item = input("Welches Item soll hinzugefügt werden? ")
            self.itemname = Item
            if self.itemname != "ende":
                Ressource1 = input("Welche Ressource ? ")
                Ressource1anzahl = input("Anzahl")

                Ressource2 = input("Welche Ressource ? ")
                Ressource2anzahl = input("Anzahl")

                Ressource3 = input("Welche Ressource ? ")
                Ressource3anzahl = input("Anzahl")

                Ressource4 = input("Welche Ressource ? ")
                Ressource4anzahl = input("Anzahl")

                ItemBP = []
                ItemBP.append(Item)
                ItemBP.append(Ressource1)
                ItemBP.append(Ressource1anzahl)
                ItemBP.append(Ressource2)
                ItemBP.append(Ressource2anzahl)
                ItemBP.append(Ressource3)
                ItemBP.append(Ressource3anzahl)
                ItemBP.append(Ressource4)
                ItemBP.append(Ressource4anzahl)
                if ItemBP not in self.items:
                    self.items.append(ItemBP)
        self.save_items()

    def save_items(self):
        datei = open("Items.txt","w")
            
        datei.write(str(self.items))
        datei.close()
        print("Ressourcen erfolgreich gespeichert")

Ash_Systems = Baubare_sachen()