import tkinter
import pygetwindow as gw
import keyboard


class WarframeOverlay(object):
    def __init__ (self):
        self.running = True
        self.name = "Warframe"
        self.Gamewindow = gw.getWindowsWithTitle(self.name)[0]
        self.Gamewindow_size = self.Gamewindow.size
        self.Gamewindow_pos = self.Gamewindow.topleft
        FONT = "{Verdana} 10 bold"
        txt = """
    root = Tkinter.Tk()
    text = Tkinter.Text(root, 
    highlightbackground='#0099e0'
    highlightthickness=1,
    relief=FLAT,).pack(expand=YES, fill=BOTH)
        """
        root = tkinter.Tk()

        text = tkinter.Text(root,relief=tkinter.FLAT, 
            font=FONT, height=10, bg='grey70', fg='blue')
        text.pack(expand=tkinter.YES, fill=tkinter.BOTH)

        button = tkinter.Button(root, text='Quit', bd=1, command=quit)
        button.pack(side=tkinter.BOTTOM, fill=tkinter.X)

        text.insert(tkinter.END, txt)
        Abmessungen = str(self.Gamewindow_size[0])+"x"+str(self.Gamewindow_size[1])+"+"+str(self.Gamewindow_pos[0])+"+"+str(self.Gamewindow_pos[1])
        #print(Abmessungen)
        #root.geometry("450x250+199+400")
        root.geometry(Abmessungen)
        root.overrideredirect(1) # fenster ohne aussen rum :-)

        root.attributes('-alpha', 0.1) # fenster transparent
        root.attributes('-topmost', 1) # fenster immer im vordergrund
        root.mainloop()

    def run(self):
        while self.running:
            if keyboard.is_pressed("k"):
                self.running = False
            self.Gamewindow = gw.getWindowsWithTitle(self.name)[0]
            self.Gamewindow_size = self.Gamewindow.size
            self.Gamewindow_pos = self.Gamewindow.topleft
            Abmessungen = str(self.Gamewindow_size[0])+"x"+str(self.Gamewindow_size[1])+"+"+str(self.Gamewindow_pos[0])+"+"+str(self.Gamewindow_pos[1])
            root.geometry(Abmessungen)
            self.run()
Overlay = WarframeOverlay()
Overlay.run()