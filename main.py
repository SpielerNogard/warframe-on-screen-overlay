import pygame
import pygetwindow as gw
import pywinauto

import keyboard
import win32api
import win32con
import win32gui




class Overlay(object):
    def __init__(self):
        self.name = "Warframe"
        pygame.init()
        self.screen = pygame.display.set_mode((800, 600),pygame.NOFRAME) # For borderless, use pygame.NOFRAME
        pygame.display.set_caption('Warframe Overlay')
        fuchsia = (255, 0, 128)  # Transparency color
        self.fuchsia = fuchsia
        self.dark_red = (139, 0, 0)

        # Set window transparency color
        hwnd = pygame.display.get_wm_info()["window"]
        win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE) | win32con.WS_EX_LAYERED)
        win32gui.SetLayeredWindowAttributes(hwnd, win32api.RGB(*fuchsia), 0, win32con.LWA_COLORKEY)

    
        self.show = False
        self.running = True
        self.run()
    def get_gameWindow(self):
        self.Gamewindow = gw.getWindowsWithTitle(self.name)[0]
        self.Gamewindow_size = self.Gamewindow.size
        self.Gamewindow_pos = self.Gamewindow.topleft
        #print("Warframe Width: "+str(self.Gamewindow_size[0]))
        #print("Warframe Height: "+str(self.Gamewindow_size[1]))

        #print("Warframe Top: "+str(self.Gamewindow_pos[1]))
        #print("Warframe Left: "+str(self.Gamewindow_pos[1]))

        self.Overlay_Window = gw.getWindowsWithTitle("Warframe Overlay")[0]
        self.Overlay_Window.moveTo(self.Gamewindow_pos[0],self.Gamewindow_pos[1])

    def run(self):
        while self.running:
            self.get_gameWindow()
            
            
            self.screen = pygame.display.set_mode((self.Gamewindow_size[0],self.Gamewindow_size[1]),pygame.NOFRAME)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
            self.screen.fill(self.fuchsia)  # Transparent background
            if self.show:
                self.paintoverlay()
            
            pygame.display.update()
            if keyboard.is_pressed("k"):
                self.running = False
            if keyboard.is_pressed("left ctrl + l"):
                if self.show == False:
                    self.show = True
                elif self.show == True:
                    self.show = False
            #self.Gamewindow.activate()
            if self.Overlay_Window.isActive == False:
                #self.Overlay_Window.minimize()
                #self.Overlay_Window.maximize()
                print("Done")


    def paintoverlay(self):
        pygame.draw.rect(self.screen, self.dark_red, pygame.Rect(30, 30, 60, 60))


Overlay = Overlay()


